package reti.communicator;

import java.net.*;
import java.io.*;

public class Communicator{

	private static final int portaTCP = 3500;
	private static final int  portaMC = 2000;

	private Socket socketClient;
	private SocketListener socketListener;
	private MulticastListener multicastListener;




	public Communicator(){

		try{

			socketListener = new SocketListener(portaTCP);
			multicastListener = new MulticastListener(portaMC, portaTCP);

			socketListener.start();
			multicastListener.start();

		}catch(Exception e){

			e.printStackTrace();

		}//try

	}//Costruttore

	public void sendMessage(String messaggio, String ipDestinatario, int porta){

		try{

			socketClient = new Socket(ipDestinatario, porta);
			PrintWriter out = new PrintWriter( socketClient.getOutputStream() );

			out.println(messaggio);

			out.close();
			socketClient.close();

		}catch(Exception e){

			e.printStackTrace();

		}//try

	}//sendMessage

	public void sendMcastDatagram(){

		while(true){

			try{

				@SuppressWarnings("resource")
				MulticastSocket mc = new MulticastSocket();
				InetAddress indirizzoMulticast = InetAddress.getByName("230.0.0.1");
				byte[] buffer = new byte[256];

				InetAddress indirizzoIP = InetAddress.getLocalHost();
				String messaggioMc = indirizzoIP.getHostAddress() + " " + portaTCP;

				buffer = messaggioMc.getBytes();

				DatagramPacket pacchetto = new DatagramPacket(buffer, buffer.length, indirizzoMulticast, portaMC);
				mc.send( pacchetto );
				
				Thread.sleep(10000);


			}catch(Exception e){

				e.printStackTrace();

			}//try
			
		}//while	
		
	}//sendMcastDatagram

}//Communicator