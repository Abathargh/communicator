package reti.communicator;

import java.util.*;

public class ProvaMain{
	
	public static void main(String...args) throws InterruptedException{
		
		
		Communicator c = new Communicator();
		new Thread(){
			public void run(){ c.sendMcastDatagram(); }
		};
		
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);

		while(true){
			
			System.out.print("Scrivi a: ");
			String destinatario =  s.nextLine();
			System.out.println();
			
			System.out.print("Sulla porta: ");
			String porta =  s.nextLine();
			System.out.println();
			
			System.out.print(">");
			String messaggio =  s.nextLine();
			System.out.println();
			
			c.sendMessage(messaggio, destinatario, Integer.parseInt(porta));
			Thread.sleep(1000);
			
		}//while
		
	}//main
	
}//ProvaMain