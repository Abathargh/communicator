package reti.communicator;

import java.net.*;
import java.io.*;


public class MulticastListener extends Thread{
	
	private int portaMC;
	private int portaTCP;	
	
	public MulticastListener(int portaMC, int portaTCP){

		this.portaMC = portaMC;
		this.portaTCP = portaTCP;

	}//Costruttore

	@Override
	public void run(){

		while(true){

			try{
				
				MulticastSocket mc = new MulticastSocket(portaMC);
				InetAddress gruppo = InetAddress.getByName("230.0.0.1");
				mc.joinGroup(gruppo);
				
				byte[] buffer = new byte[256];
				DatagramPacket pacchetto = new DatagramPacket(buffer, buffer.length);
				
				mc.receive(pacchetto);
				
				String messaggio = new String( pacchetto.getData() );
				
				System.out.println(messaggio);
				
				if(pacchetto.getAddress().equals(InetAddress.getLocalHost()))
					continue;
				
				Socket s = new Socket(pacchetto.getAddress().getHostAddress(), pacchetto.getPort());
				PrintWriter out = new PrintWriter( s.getOutputStream() );
				
				out.println("Connesso al gruppo: " + InetAddress.getLocalHost().getHostName() + " " + portaTCP);
				
				s.close();
				mc.close();
				
			}catch(Exception e){

				e.printStackTrace();

			}//try

		}//while

	}//run

}//MulticastListener
