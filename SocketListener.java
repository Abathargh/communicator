package reti.communicator;

import java.net.*;
import java.io.*;

public class SocketListener extends Thread{
	
	private ServerSocket server;
	
	public SocketListener(int portaTCP){
		
		try{
			
			server = new ServerSocket(portaTCP);
			
		}catch(Exception e){
			
			e.printStackTrace();
			
		}//try
		
	}//Costruttore
	
	
	@Override
	public void run(){
		
		try{
			
			Socket mittente = server.accept();
			BufferedReader in = new BufferedReader( new InputStreamReader( mittente.getInputStream() ) );
			
			String messaggio = in.readLine();
			
			System.out.println(mittente.getInetAddress().getHostName() + ">" + messaggio);
			
			in.close();
			mittente.close();
			
		}catch(Exception e){
			
			e.printStackTrace();
			
		}//try
		
	}//run
	
}//SocketListener